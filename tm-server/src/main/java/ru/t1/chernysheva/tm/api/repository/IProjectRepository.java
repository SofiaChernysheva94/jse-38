package ru.t1.chernysheva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    @NotNull
    Project update(@NotNull Project model) throws Exception;

}
