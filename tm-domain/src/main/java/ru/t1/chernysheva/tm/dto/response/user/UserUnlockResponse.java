package ru.t1.chernysheva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserUnlockResponse extends AbstractUserResponse {

    private User user;

    public UserUnlockResponse(User user) {
        this.user = user;
    }

}
